/* INSERSOR DE TEXTO JAVASCRIPT */

var editor_options = document.getElementById("text-editor-options").childNodes;
var editor = document.getElementById("text-editor");
var text_container = document.getElementById("text-container");
var submit_button = document.getElementById("send-button");

for(var opt of editor_options)
{
	opt.addEventListener('click', function() {
		this.classList.toggle("active");
	});
}

submit_button.addEventListener('click', function() {
	var text = editor.value;
	editor.value = "";

	if(text.length == 0)
		return;

	for(var opt of editor_options)
	{
		if(opt.nodeName == 'BUTTON' && opt.classList.contains('active'))
		{
			switch(opt.id)
			{
				case 'bold':
					text = `<b>${text}</b>`;
				break;

				case 'italic':
					text = `<i>${text}</i>`;
				break;

				case 'underline':
					text = `<u>${text}</u>`;
				break;
			}	
		}
	}

	text_container.innerHTML = text_container.innerHTML + `<pre>${text}</pre><hr>`;
});